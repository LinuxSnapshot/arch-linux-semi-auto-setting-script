# Arch linux Semi-Auto Setting Script

아치 리눅스용 반자동 설정 스크립트입니다.
1. packagekit-qt5 및 plasma-wayland-session 설치

2. Zsh 설치 후 테마, 플러그인 설정

3. VSCodium, rustup, kime 입력기, xed 설치 (kime 세팅은 https://github.com/Riey/kime/blob/develop/README.ko.md#%EA%B7%B8%EC%99%B8 참고)

## License
MIT License에 따라 라이선스가 부여됩니다.
