## Plasma
yay -S packagekit-qt5 --noconfirm && yay -S plasma-wayland-session --noconfirm

## Zsh install & Theme
yay -S zsh --noconfirm && chsh -s /usr/bin/zsh && git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k && echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc

## Zsh Auto Suggestion
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions && echo 'source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh' >>~/.zshrc

## Zsh Syntax Highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git && echo "source ${(q-)PWD}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ${ZDOTDIR:-$HOME}/.zshrc

## Other
yay -S vscodium-bin --noconfirm && yay -S rustup --noconfirm && yay -S kime-bin --noconfirm && yay -S xed --noconfirm && sudo xed /etc/environment && reboot
